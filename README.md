# This package is no longer maintained. Please use the julia implementation instead: [https://github.com/NilsKintscher/alfa.jl]

# aLFA : automated local Fourier analysis

This python3-package **aLFA** (automated local Fourier analysis) is an open source implementation of the framework described in 
> Kahl, K., Kintscher, N. Automated local Fourier analysis (aLFA). Bit Numer Math (2020). [https://doi.org/10.1007/s10543-019-00797-w]

The main purpose of this framework is to enable the reliable and easy-to-use analysis of complex methods on repetitive structures, e.g.,  multigrid methods with complex overlapping block smoothers.

Throughout this framework, the term *aLFA_Article* refers to https://doi.org/10.1007/s10543-019-00797-w.

### Warning: This is still a preliminary version. No unit tests have been written yet. Although the framework was written for any number of dimensions, so far only the two-dimensional case has been tested. Furthermore, a refactorisation of major parts of the code could happen in the future. 

## Installation:
It is recommended to use conda and to use a separate conda environment:
1. Install miniconda (python3) from https://docs.conda.io/en/latest/miniconda.html
2. aLFA depends on several packages listed in conda-alfa.yml. You can install them via  

```
conda env create -f conda-alfa.yml
``` 
from the anaconda *standard* and *conda-forge* repository which creates a conda environment named **alfa**. This environment can then be activated via  
```
conda activate alfa
``` 
(The package *jupyter* is not mandatory but recommended as the examples are Jupyter Notebooks)

3. Some files are written in cython and thus need to be compiled. This can be done via  
```
make
```  
within the main directory. To this end, a C and a C++ compiler is necessary. (Do not worry about the warning "Using deprecated NumPy API", see https://cython.readthedocs.io/en/latest/src/userguide/source_files_and_compilation.html#warnings)

4. You might consider adding your alfa path to your $PYTHONPATH variable via    
```
export PYTHONPATH=/path/to/alfa/:$PYTHONPATH
```    
Now you can use the framework after loading it as usual via  
```python 
import alfa
```  


## Examples

The examples are Jupyter Notebooks, see https://jupyter.org/. To inspect the examples change to the *alfa/examples/* folder and use the command
```
jupyter notebook
``` 
This should open a new browser window/tab in which you can inspect the Notebook. We recommend to study the notebooks in order to get a first impression of the structure of this framework. Examples 1, 2 and 3 are part of the discussion in *aLFA_Article*.
- *01_Tutorial_Laplacian_2D_twogrid.ipynb* contains the analysis of the Laplace operator discretized on an equidistant rectangular lattice. By analyzing several different smoothers this example is a good starting point to get familiar with several functions of this framework, see Sections 2-4.
- *02_Graphene_FourColorOverlap.ipynb* contains the analysis of the multicolored block smoother (and two-grid method) for the tight-Binding Hamiltonian of graphene, see Section 6.1.
- *03_CurlCurl_HalfHybrid.ipynb* contains the two-grid analysis for the curl-curl equation using the auxiliary space hybrid smoother, reproducing the results found in *Local Fourier Analysis of Multigrid for the Curl-Curl Equation* (Tim Boonen, Jan Van Lent, and Stefan Vandewalle, https://epubs.siam.org/doi/abs/10.1137/070679119), see Section 6.2.
- *04_CurlCurl_AFW_4ColorOverlap.ipynb* contains the analysis of the Arnold-Falk-Winther-smoother for the curl-curl equation, reproducing the results of *Local Fourier Analysis of Multigrid for the Curl-Curl Equation* (Tim Boonen, Jan Van Lent, and Stefan Vandewalle, https://epubs.siam.org/doi/abs/10.1137/070679119).
- *05_Laplacian_Hexagonal.ipynb* contains an analysis of the Laplace operator discretized on a hexagonal lattice and reproduced several results found in *Fourier Analysis of Multigrid Methods on Hexagonal Grids* (Guohua Zhou and Scott R. Fulton, https://epubs.siam.org/doi/10.1137/070709566).

## Structure of the Framework

This is a class-based implementation of the framework described in *aLFA_Article*. We give a quick overview of the most important classes, attributes and functions, where we refer to Definitions/Theorems and Algorithms of *aLFA_Article*.

### Lattice: 
This represents a Bravais lattice, see Definition 2.1.
- Constructor:
    - ```__init__(A)``` sets self.A to A.
- Attributes: 
    - ```numpy.ndarray A``` corresponds to the lattice basis (2dimensional numpy.ndarray of size dim x dim).
- Functions:
    - ```integer dim``` returns the dimension of the lattice, i.e. self.A.shape[0]. 
    - ```Lattice lcm(Lattice other)``` corresponds to Algorithm B.7 and returns the least common multiple lattice basis of self and other.
    - ```numpy.ndarray elements_in_quotientspace(Lattice other)``` corresponds to Algorithm B.3 and returns the elements of the quotient space self/other.
    - ```void plot_lattice(...)``` plots the lattice.
### Crystal:
This class inherits from lattice and defines N different crystals, see Definition 2.3.
- Constructor:
    - ```__init__(A,s)``` sets self.A to A and self.s to s.
- Attributes:
    - ```numpy.ndarray A``` corresponds to the underlying lattice basis.
    - ```list s``` is a list of N structure elements. Each self.s[i] is of type numpy.ndarray and corresponds to a single structure element. Together with self.A it defines a crystal. 
- Functions:
    - ```void plot_crystal_all()``` plots all Crystals, i.e., all points  self.A + self.s[i] for all i.

### Multiplier:
This class represents a single multiplier of a multiplication operator, see Theorem 3.1.
- Constructor:
    - ```__init__(matrix, pos)``` sets self.matrix to matrix and self.pos to pos.
- Attributes:
    - ```numpy.ndarray matrix``` represents the multiplication matrix at self.pos. 
    - ```numpy.ndarray pos``` integral position (fractional coordinate) of multiplier (crtesian coordinate = C.A@pos).
### Operator:
This class represents a multiplication operator, see Theorem 3.1. Note that
 (Of)(x) = sum_{i in range(len(m))} m[i].matrix f(x+ m[i].pos)
- Constructor:
    - ```__init__(Crystal C)``` sets self.C to C. To define the operator simply use the function self.add_multiplier.
- Attributes: 
    - ```Crystal C``` this must be a crystal with len(C.s)==2. The crystal C.A + C.s[0] defines the domain of the operator. C.A + C.s[1] defines the co-domain of the operator.
    - ```List m``` list of non-zero multipliers.
- Functions:
    - ```void add_multiplier(matrix, pos)``` creates a multiplier and adds it to the list of multipliers m.
    - ```Operator wrt_lattice(Lattice L)``` rewrites this operator with respect to the sublattice L.A, see Algorithm B.4.
    - ```Operator change_structure_elements(s)``` rewrites this operator with respect to the domain self.C.A + s[0] and codomain self.C.A + s[1], see Algorithm B.6.
    - ```void Normalize()``` normalizes this operator, i.e., the structure elements are shifted into self.C.A[0,1)^dim and sorted lexicographically, see Algorithm B.2.
    - ```(Operator, Operator) make_operators_compatible(other)``` rewrites both operators, self and other, with respect to a single lattice and normalizes their structure elements, see Algorithm B.5. The following functions make internal use of this function.
        - ```Operator __add__(Operator other)``` defines the "+" operator for multiplication operators, see Lemma A.1.(i).
        - ```Operator __sub__(Operator other)``` defines the "-" operator for multiplication operators, see Lemma A.1.(i).
        - ```Operator __matmul__(Operator other)``` defines the "@" operator for the computation of self@other, see Lemma A.1.(ii).
    - ```Operator __mul__(scalar other)``` defines the "*" operator for a scalar times a multiplication operator.        
    - ```Operator __transpose__()``` returns the transpose of self, see Lemma A.1.(iii).
    - ```symbol(k)``` returns the symbol S_k of this operator, see Definition 3.3.
    - ```plot()``` plots the stencil of the operator. The coloring corresponds to the real part of the matrix entries.

### Operator_Composition:
Corresponds to a composition of multiplication operators and is used to analyze such a composition, e.g., to compute and plot its eigenvalues, see Algorithm B.1. Several functions essentially execute the string f, see ```__init__(X,f)```. This functionality may be replaced with a suitable parser in a future version.
- Constructor:
    - ```__init__(X,f,wrtLattice=None)``` X is a dictionary of operators, f denotes the composition function and, optionally, wrtLattice is an alfa.Lattice. A simple/typical example is X={'I':alfa.Operator, 'S': alfa.Operator, 'L':alfa.Operator} and f="('I' - pinv('S')@'L')" which corresponds to (X['I'] - pinv(X['S'])@X['L']). This constructur calls the function `self._rewrite_wrt_common_sublattice(wrtLattice)` (see Algorithm B.5.) and afterwards `_check_compatibility(wrtLattice)`.
- Attributes:
    - ```Crystal C``` this crystal corresponds to the underlying crystal, which was found within `_rewrite_wrt_common_sublattice(wrtLattice)` within the constructor ```__init__```.
    - ```pandas.DataFrame df``` this DataFrame consists of wavevectors and eigenvalues of the operator composition f(X). It is created as soon as `discretize_frequency_space()` or `discretize_frequency_space_wrt_torus()` is called.
- Functions:
    - ```_rewrite_wrt_common_sublattice(wrtLattice)``` rewrites all operators with respect to a single least common multiple lattice and normalizes all operators, see Algorithm B.5. If wrtLattice is not None, this sublattice is set to wrtLattice instead of constructing a common sublattice automatically.  Afterwards it calls `_check_compatibility(wrtLattice)`.
    - ```_check_compatibility(wrtLattice)``` this function checks if all domains and codomains are compatible such that the expression *self.f* makes sense and can be computed.
    - ```discretize_frequency_space(N)``` this function is used to discretize the primitive cell A^{-T}[0,1)^dim of the frequency space. It sets up the Dataframe-columns df["k_i"], i=0,1,...,dim-1,  by discretizing [0,1)^dim into N^dim equidistant points. After that, the actual discretization of the primitive cell is saved in df["dAk_i"], i=0,1,...,dim-1. This discretization is equal to T_{self.L.A, N * self.L.A}*, see Theorem 3.4.
    - ```discretize_frequency_space_wrt_torus(Lattice L)``` same as ```discretize_frequency_space(N)```, but using exactly the wavevectors k in T_{self.L.A, L.A}* instead of T_{self.L.A, N * self.L.A}*, see Theorem 3.4.
    - ```compute_spectrum(N)``` computes the spectrum f(X_k) for all wavevectors k in the current dataframe self._df["dAk_i"] if N is None. If N is an integer, ```discretize_frequency_space(N)``` is called beforehand. These eigenvalues are saved within the Dataframe. (Furthermore, a smoothing analysis can be done using this function. We have not yet properly documented how this is done. However, this features finds application in example 04 and 05.)
    - ```plot_spectrum()``` creates a contour plot of the spectral radii of this operator based on self.df
        
