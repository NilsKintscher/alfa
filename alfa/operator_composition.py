# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""


import numpy as np
import scipy.linalg as linalg
from itertools import product
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # 3d plot
import matplotlib.tri as tri  # triangulation used for contourplot
from matplotlib.ticker import LinearLocator, FormatStrFormatter

import alfa.operator
from alfa.functions import sym_pinv, _fake_pinv, lll, isintegral
from alfa.c_functions import find_permutation_and_shifts


import copy


class Operator_Composition(object):
    """
    Operator composition of multiplication operators C:T_{A}^s[0] -> T_{A}^s[1]
    with (Cf)(x) = sum_y m(y)f(x+Ay)

    This class is mainly used to compute the eigenvalues of C and realizes Algorithm B.1 of aLFA_Article.
    The function discretize_frequency_space(N) computes the eigenvalues of the symbols C_k with k in
    A^{-T}[0,1)^dim. The wavevectors k and eigenvalues are saved in a
    pandas.DataFrame .df

    Parameters
    ----------
    X:  Dictionary of alfa.Operators, f.e. {'I': alfa.Operator, 'S': alfa.Operator, 'L': alfa.Operator}
    f:  Expression, f.e.
        "('I' - pinv('S')@'L')"
        which corresponds to (X['I'] - pinv(X['S'])@X['L']) using the dictionary X.

    wrtLattice: alfa.Lattice, optional argument.
        All operators of the dictionary X are rewritten with respect to this translational invariance. Thus, it has to be a sublattice of all translational invariances of all operators in the dictionary X.
        In case wrtLattice=None, a common sublattice is automatically chosen.
    """

    def __init__(self, X, f, wrtLattice=None):
        """
        This constructur calls the function self._rewrite_wrt_common_sublattice() (See Algorithm B.5.) and afterwards _check_compatibility().
        """
        self._O = X
        self._parse_f(f)

        self._rewrite_wrt_common_sublattice(wrtLattice)
        self._check_compatibility(wrtLattice)

        self._df = None

    def _parse_f(self, f):
        self._f_orig = f
        if f[0] == '@':
            f = f[1:]
        if f[-1] == '@':
            f = f[:-1]

        # replace every occurance of '?' with self._O['?']

        s_splitted = f.split("'")
        # splitting s_splitted in rest + operators.
        s_rest = s_splitted[::2]
        s_keys = s_splitted[1::2]
        self.used_dict_keys = copy.deepcopy(s_keys)

        # modify operator occurances #1
        keys_f = ["self._O['" + x + "']" for x in s_keys]
        keys_f_sym = ["S['" + x + "']" for x in s_keys]

        # rejoin strings
        joined_list = [None] * (len(s_rest) + len(keys_f))
        joined_list[::2] = s_rest

        joined_list[1::2] = keys_f
        f = ''.join(joined_list)  # convert list of strings to string
        self._f = f.replace("pinv(", "alfa._fake_pinv(")

        joined_list[1::2] = keys_f_sym
        f = ''.join(joined_list)  # convert list of strings to string
        self._f_sym = f.replace("pinv(", "alfa.sym_pinv(")

        # used in self._check_compatibility
        self._f_as_func = eval("lambda: " + self._f)
        self._f_sym_as_func = eval("lambda: " + self._f_sym)  # used in

    @property
    def shape(self):
        """
        returns the shape of the symbol
        """
        return (self.C.s[0].shape[0],
                self.C.s[1].shape[0])

    @property
    def operatorlist(self):
        return self._O

    @property
    def df(self):
        """
        Access to the complete DataFrame where the wavevectors and eigenvalues are
        saved.
        """
        return self._df

    @property
    def df_k(self):
        """
        DataFrame of wavevectors k in [0,1]^dim.
        """
        assert self._df is not None, "DataFrame df not initialized. Call self.discretize_frequency_space or discretize_frequency_space_wrt_torus"
        return self._df.iloc[:, 0:self.C.dim]

    @property
    def df_dAk(self):
        """
        DataFrame of wavevectors k in A^{-T}[0,1]^dim.
        """
        assert self._df is not None, "DataFrame df not initialized. Call self.discretize_frequency_space or discretize_frequency_space_wrt_torus"
        return self._df.iloc[:, self.C.dim:2 * self.C.dim]

    @property
    def df_eigs(self):
        """
        DataFrame of eigenvalues corresponding to the wavevector k in df_k.
        """
        assert self._df is not None, "DataFrame df not initialized. Call self.discretize_frequency_space or discretize_frequency_space_wrt_torus"
        return self._df.iloc[:, 2 * self.C.dim:]

    def _rewrite_wrt_common_sublattice(self, wrtLattice):
        """
        Rewrites all operators w.r.t a single lattice basis and normalizes the operators.
        """
        if wrtLattice is None: # if wrtLattice is not set, a common sublattice is automatically chosen.
            for key in self.used_dict_keys:
                if wrtLattice is None:
                    wrtLattice = self._O[key].C
                else:
                    wrtLattice = wrtLattice.lcm(self._O[key].C)

        for key in self.used_dict_keys:
            self._O[key] = self._O[key].wrt_lattice(wrtLattice)
            self._O[key].normalize()

    def _check_compatibility(self, wrtLattice):
        """
        This function can be used to check if the expression f can be computed,
        i.e., if a single common lattice basis exists for all operators and if
        the codomains and domains are compatible.
        """
        for key in self.used_dict_keys:
            self._O[key]._compatibility_check_only = True
        try:
            O = eval(self._f)
            if wrtLattice is not None:
                O = O.wrt_lattice(wrtLattice)
            else:
                O.C.A = lll(O.C.A)
            O.normalize()  # normalize structure elements.
            self.C = O.C

        except BaseException:
            raise Exception("Compatibility check failed!")
        else:
            pass
        finally:
            for key in self.used_dict_keys:
                self._O[key]._compatibility_check_only = False

    def discretize_frequency_space_wrt_torus(self, L):
        """
        Setup the dataframe self.df which contains all wavevectors k in T_{A,Z}^*
        where A = self.C.A and Z=L.A.

        alfa.lattice L: This should be a sublattice Z of self.C.A, i.e.,
            (self.C.A)^{-1}*L.A has to be integral.
        """
        lda = alfa.Lattice(self.C.dA)
        ldz = alfa.Lattice(L.dA)
        s = ldz.elements_in_quotientspace(lda)@lda.iA.T

        # T_{A,Z}* = L(A)* / L(Z)*
        # M = A^{-1}Z
        self._setup_dataframe(s)

    def discretize_frequency_space(self, N, lim=None):
        """
        The frequency space A^{-T}[lim[0],lim[1])^dim is discretized into (N)^dim equidistant points.
        This discretization is saved in the dataframe self.df.

        If lim=None, then lim[0]=0 and lim[1]=1 is used.
        """

        if lim is None:
            lim = [0, 1]

        ls = []
        for i in range(0, self.C.dim):
            ls.append(np.linspace(lim[0], lim[1], N, False))

        disc01 = np.asarray(list(product(*ls)))
        # add these points to the dataframe self._df
        self._setup_dataframe(disc01)

    def _setup_dataframe(self, disc_set):
        """
        Setup of the dataframe self._df including the computation of the eigenvalues
        of C_k for all k.
        """
        dfk = pd.DataFrame(
            disc_set,
            columns=["k_" + str(x) for x in range(self.C.dim)]
        )
        dfdAk = pd.DataFrame(
            disc_set@self.C.dA.T,
            columns=["dAk_" + str(x) for x in range(self.C.dim)]
        )
        self._df = dfk
        self._df = pd.concat([self._df, dfdAk], axis=1, copy=False, sort=None)

    def compute_spectrum(self, N=None, C=None, num_lowest_freq=0):
        """
        Computes the spectrum f(X_k) for all wavevectors k in the current dataframe self._df["dAk_i"] if N is None.
        (The Dataframe can be setup with self.discretize_frequency_space or self.discretize_frequency_space_wrt_lattice )
        N: If N is not None:
            self.discretize_frequency_space(N) is called to setup the dataframe containing
            (N)^dim equidistant points of A^{-T}[0,1)^dim.


        The arguments C and num_lowest_freq can be used for a smoothing analysis.
        WARNING: The smoothing analysis has hardly been tested.

        C: alfa.Crystal C, used to transform the symbol with respect to a basis with different frequencies which makes a smoothing analysis (e.g for block smoothers) possible.
            The underlying lattice spanned by self.C.A has be a sublattice of C.A. The basis consists of wavefunctions with frequencies
            (k +k_1), (k+k_2), .., (k+k_p), where \{k_1,...,k_p\} \cong T_{C.A, self.C.A}^* correspond to representatives of the dual lattice torus.

        num_lowest_freq: filtering out the num_lowest_freq smallest frequencies k+k_{i_1}, ..., k+k_{i_{num_lowest_freq}}. (smoothing analysis).
        """
        if N is not None:
            self.discretize_frequency_space(N)

        assert self._df is not None, "DataFrame df not initialized. Call self.discretize_frequency_space or discretize_frequency_space_wrt_torus"

        if C is not None:  # Setup phase for the smoothing analysis.
            #
            # This complete step can and should significantly be improved. (Performance wise)
            #

            # Compute points in T_{C.A, self.C.A}
            e = C.elements_in_quotientspace(self.C)

            # List of different frequencies, points in T_{C.A, self.C.A}^*
            # todo: directly compute it from e compute from e instead of
            k_shifts = alfa.Lattice(
                self.C.dA).elements_in_quotientspace(
                alfa.Lattice(
                    C.dA))

            # Define the structure element s = x + C.s
            s0 = C.s[0]
            u0 = np.tile(e, (1, s0.shape[0])).reshape(-1, C.dim) \
                + np.tile(s0, (e.shape[0], 1)).reshape(-1, C.dim)

            # Determine permutation p, s.t. u0[i] = self.C.s[0][p[i]]. < check
            perm_p = find_permutation_and_shifts(self.C, u0, self.C.s[0])[0]

            # Construct permutation matrix.
            perm_mat = np.eye(self.C.s[0].shape[0], dtype='complex64')
            perm_mat = perm_mat[perm_p]

            def compute_freq_basis_subblock(k, i, j):
                """
                This function is used to create the frequency basis needed for a smoothing analysis in get_freq_basis.

                k: frequency
                i: block row
                j: block column
                """
                return np.diag(np.asarray([np.exp(
                    1j * 2 * np.pi * (k + k_shifts[j]).dot(e[i] + t)) for t in C.s[0]], dtype='complex64'))

            def get_freq_basis(k):
                """
                This function creates the frequency basis used in the smoothing analysis.
                """
                block_cols = []
                for i in range(e.shape[0]):
                    block_cols.append(np.hstack([compute_freq_basis_subblock(
                        k, i, j) for j in range(k_shifts.shape[0])]))
                block_mat = np.vstack(block_cols)

                for j in range(block_mat.shape[0]):
                    block_mat[:, j] = block_mat[:, j] / \
                        np.linalg.norm(block_mat[:, j])
                return block_mat

            corner_list = []
            for i in range(0, self.C.dim):
                # I still have to check if [0,1] would be enough. Until then, I use simply more lattice points nearby.
                corner_list.append([-3, -2, -1, 0, 1, 2, 3])
            corner_list = np.asarray(list(product(*corner_list)))

            def get_filter_matrix_of_lowest_freq(k):
                """
                This function creates the matrix which simply filters out low frequencies
                k+k_{i_1}, ..., k+k_{i_{num_lowest_freq}}

                """
                freq_list = [k + ks for ks in k_shifts]
                kshiftpk_distance_to_corner = []
                for kk in freq_list:

                    kshiftpk_distance_to_corner.append(np.min([np.linalg.norm(
                        kk - C.dA@corners) for corners in corner_list]))

                k_sortidx = np.argsort(kshiftpk_distance_to_corner)

                filter_mat = np.eye(self.C.s[0].shape[0], dtype='complex64')
                for i in range(num_lowest_freq):
                    idx_from = k_sortidx[i] * C.s[0].shape[0]
                    idx_to = idx_from + C.s[0].shape[0]
                    filter_mat[idx_from:idx_to, idx_from:idx_to] = 0

                return filter_mat

        # Compute symbols & eigenvalues w.r.t dAk
        eigarr = np.empty(
            [self._df.shape[0], self.shape[0]], dtype='complex64')
        eigarr[:] = None
        eigdf = pd.DataFrame(
            eigarr,
            columns=["eig_" + str(x) for x in range(self.shape[0])]
        )
        self._df = pd.concat([self._df, eigdf], axis=1, copy=False, sort=None)

        evaluate_expr = eval("lambda S: " + self._f_sym)
        for idx, k in enumerate(self.df_dAk.values):

            S = {}
            for key in self.used_dict_keys:
                S[key] = self._O[key].symbol(k)
            symbol = evaluate_expr(S)

            if C is not None:  # Application of the smoothing analysis.

                #
                # This complete step can and should significantly be improved. (Performance wise)
                #

                # create a basis using different frequencies.
                symbol_permuted = perm_mat@symbol@perm_mat.T
                freq_basis = get_freq_basis(k)
                symbol_wrt_freq_basis = np.linalg.inv(
                    freq_basis)@symbol_permuted@freq_basis

                # Filter out the lowest frequencies.
                filter_mat = get_filter_matrix_of_lowest_freq(k)
                symbol_filtered = filter_mat@symbol_wrt_freq_basis
                eigs = linalg.eig(symbol_filtered)[0]
            else:
                eigs = linalg.eig(symbol)[0]
            # insert eigenvalues sorted from largest to smallest
            eigarr[idx, :] = eigs[np.argsort(-abs(eigs))]

    def compute_eigenvalue(self, k):
        """
        Computes eigenvalues of f(X_k) w.r.t. a single wavevector k.
        """
        evaluate_expr = eval("lambda S: " + self._f_sym)
        #S = []
        # for sidx, O in enumerate(self._O):
        #    S.append(O.symbol(k))
        S = {}

        for key in self.used_dict_keys:
            S[key] = self._O[key].symbol(k)
        symbol = evaluate_expr(S)
        #symbol = evaluate_expr(S)
        eigs, v = linalg.eig(symbol)
        
        sortidx = np.argsort(-abs(eigs))
        eigs = eigs[sortidx]
        v = v[:,sortidx]
        # return eigenvalues sorted from largest to smallest
        return eigs, v

    def plot_spectrum(self, zlim=np.inf, levels=8):
        """
        Simply a wrapper for plot_spectrum_contourf. Plots the spectral radius.
        """
        if self.C.dim == 2:
            self.plot_spectrum_contourf(zlim, 0, levels)
        elif self.C.dim == 1:
            x = self.df_dAk.values[:, 0]
            for idx in range(self.C.s[0].shape[0]):
                z = np.abs(self.df_eigs.values[:, idx])
                plt.plot(x, z)

    def plot_spectrum_contourf(self, zmax=np.inf, idx=None, levels=8):
        """
        A contourf plot of the spectrum. (Only the absolut part of the largest eigenvalues)

        zmax: values below zmax are filtered.
        idx: index of column self.df_eigs.values[:, idx] which is plotted.
             In case idx=None the spectral radius is plotted.
        levels: number of levels in the contourplot
        """
        if idx is None:
            idx = 0
        if self.C.dim == 2:
            x = self.df_dAk.values[:, 0]
            y = self.df_dAk.values[:, 1]
            z = np.abs(self.df_eigs.values[:, idx])

            # filter nan values.
            mask = np.ma.masked_invalid(z)
            x = x[~mask.mask]
            y = y[~mask.mask]
            z = z[~mask.mask]

            # filter zlim
            mask2 = np.where(np.abs(z) < zmax)
            x = x[mask2]
            y = y[mask2]
            z = z[mask2]

            triang = tri.Triangulation(x, y)
            plt.gca().set_aspect('equal')
            contourf = plt.tricontourf(triang, z, levels)
            plt.colorbar(contourf, shrink=1, aspect=10)
            plt.tricontour(triang, z, levels, colors='k')

            plt.title("max(abs(eigvals)) = " + str(np.max(z)) + " \n at k=" +
                      # str(self.df_k.loc[np.abs(self.df_eigs).idxmax(axis=0)[0],
                      # :].tolist()) + " \n (A^-T)@k=" +
                      str(self.df_dAk.loc[np.abs(self.df_eigs).idxmax(axis=0)[idx], :].tolist()))

            plt.axis('equal')

        else:
            print("""Plot function not yet implemented for dim = """ + str(self.C.dim))

    def plot_spectrum_3d(self, zlim=None, idx=None):
        """
        A 3d plot of the absolute part of the eigenvalues

        zlim: used for the zlim of the axis and the colormap. zlim[0] = min, zlim[1] = max.

        idx: index of column self.df_eigs.values[:, idx] which is plotted.
             In case idx=None the spectral radius is plotted.
        """
        if idx is None:
            idx = 0
        if self.C.dim == 2:
            ax = Axes3D(plt.gcf())

            if zlim is None:
                zlim = [np.nanmin(np.abs(self.df_eigs.values[:, idx])), np.nanmax(
                    np.abs(self.df_eigs.values[:, idx]))]

            surf = ax.plot_trisurf(
                self.df_dAk.values[:, 0],
                self.df_dAk.values[:, 1],
                np.abs(self.df_eigs.values[:, idx]),
                cmap=mpl.cm.viridis,  # cbar_cmap,
                vmin=zlim[0],
                vmax=zlim[1],
            )

            ax.set_zlim(zlim[0], zlim[1])
            ax.zaxis.set_major_locator(LinearLocator(10))
            ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

            plt.gcf().colorbar(surf, shrink=.6, aspect=10)
        else:
            print("""Plot function not yet
                  implemented for dim = """ + str(self.C.dim))
