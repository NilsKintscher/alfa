# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""


from alfa.crystal import Crystal
from alfa.lattice import Lattice
import alfa.c_functions
from alfa.operator import Operator
from alfa.operator_composition import Operator_Composition
import alfa.functions
from alfa.functions import _fake_pinv, sym_pinv
from alfa.tools import *
# sub-package in folder gallery.
from alfa.gallery import *


__all__ = [
    'Crystal',
    'Lattice',
    'Operator',
    '_fake_pinv',
    'sym_pinv',
    'Operator_Composition',
    'gallery',
    'tools',
    'functions']
