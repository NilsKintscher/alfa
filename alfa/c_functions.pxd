# distutils: language = c++
# cython: language_level=3
# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""
#from __future__ import division

#import cython

import numpy as np
cimport numpy as np

# https://flothesof.github.io/cython-complex-numbers-parallel-optimization.html
DTYPEf = np.float64
DTYPEi = np.int_
DTYPEc = np.complex128
ctypedef np.float64_t DTYPEf_t
ctypedef np.int_t DTYPEi_t
ctypedef np.complex128_t DTYPEc_t


from libc.math cimport fabs
from libc.math cimport round, floor
from libcpp cimport bool as bool_t

cdef bool_t _is_close(DTYPEf_t, DTYPEf_t)

cdef void _find_permutation_and_shifts(int,  
                                       int, 
                                       np.ndarray[DTYPEf_t, ndim=2], 
                                       np.ndarray[DTYPEf_t, ndim=2], 
                                       DTYPEi_t[:],
                                       DTYPEi_t[:,:])