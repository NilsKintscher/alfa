# distutils: language = c++
# cython: language_level=3
# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import scipy.linalg as linalg

from cypari2 import Pari
from cypari2.convert import gen_to_python

pari = Pari()
DTYPE = np.int

import cython
#from cython import array
#import array
cimport numpy as np

# https://flothesof.github.io/cython-complex-numbers-parallel-optimization.html
DTYPEf = np.float64
DTYPEi = np.int_
DTYPEc = np.complex128
ctypedef np.float64_t DTYPEf_t
ctypedef np.int_t DTYPEi_t
ctypedef np.complex128_t DTYPEc_t

cdef DTYPEf_t rel_tol = 1e-05
cdef DTYPEf_t abs_tol = 1e-08



def isclose(x, y):  # np.ndarray[DTYPE_t, ndim=2]
    """
    isclose wrapper for numpy
    """
    return np.allclose(x, y, 1e-04, 1e-07)

@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline bool_t _is_close(DTYPEf_t a, DTYPEf_t b):
    """
    check if two numbers are equal neglecting numerical inexactness:
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.isclose.html
    """
    cdef DTYPEf_t amb = fabs(a - b)
    if amb <= rel_tol * abs_tol or amb <= rel_tol * max(fabs(a), fabs(b)):
        return True
    return False


def isintegral(x):
    """
    checks if input x is integral. Returns true/false and round(x)
    """
    x = np.asarray(x)
    #xr = np.round(x)
    x_int = np.round(x).astype(DTYPE)
    return isclose(x, x_int), x_int


def smith_normal_form(A):
    """
    Wrapper to compute the Smith normal form from cypari.

    Output: S,U,V such that S=UAV

    Parameters: a matrix A as numpy.ndarray. Should be integral.
    """
    A = pari.matrix(A.shape[0], A.shape[1], A.reshape([1, -1])[0])
    (U, V, S) = A.matsnf(1)
    return np.asarray(
        gen_to_python(S)), np.asarray(
        gen_to_python(U)), np.asarray(
            gen_to_python(V))


def hermite_normal_form(A):
    """
    Wrapper to compute the Hermite normal form from cypari.

    The HNF is of the following form:
        - upper triangular triangular
        - elementwise nonnegative
        - row-wise maximum is located on the diagonal.
    """
    A = pari.matrix(A.shape[0], A.shape[1], A.reshape([1, -1])[0])
    H = A.mathnf(0)
    return np.asarray(gen_to_python(H))


def lll(A):
    """
    Wrapper for the LLL algorithm from cypari.

    Used to compute a near orthogonal and short lattice basis.
    """
    A = pari.matrix(A.shape[0], A.shape[1], A.reshape([1, -1])[0])
    A = A * A.qflll()  # qflll returns the unimodular transformation
    return np.asarray(gen_to_python(A))


def _fake_pinv(L):
    """
    Used in the compatibility check (regarding domain and codomain of the operators)
    of Operator.pinv if Operator._compatibility_check_only is True.
    """
    return L.transpose()


def sym_pinv(A):
    """
    computes the pseudoinverse of a matrix A via scipy.linalg.pinv2
    """
    return linalg.pinv2(A, 1e-15, None, False, True)


cdef _find_permutation_and_shifts():
    """
    Finds the permutation and shifts for two congruent structure elements with
    respect to a underlying translational invariance.
    This function is for example used in the function 
    operator.change_structure_elements
    """
    return 0