# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""
import numpy as np
import copy

from alfa.crystal import Crystal
from alfa.operator import Operator

from alfa.export_decorator import export


@export
def curlcurl_restriction():
    A = 2 * np.eye(2)
    s0 = np.asarray([[.5, 0], [0, .5]])
    s = [np.asarray([s0 + np.asarray([x, y]) for x in [0, 1]
                     for y in [0, 1]]).reshape(1, -1, 2)[0], 2 * s0]
    C = Crystal(A, s)
    L = Operator(C)
    L.normalize()
    m = np.zeros([2, 8])
    m[0, [0, 1]] = .5
    m[0, [4, 5]] = .25
    m[1, [2, 6]] = .5
    m[1, [3, 7]] = .25
    L.add_multiplier(m, np.asarray([0, 0]))
    m = np.zeros([2, 8])
    m[1, [3, 7]] = .25
    L.add_multiplier(m, np.asarray([0, -1]))
    m = np.zeros([2, 8])
    m[0, [4, 5]] = .25
    L.add_multiplier(m, np.asarray([-1, 0]))

    return L


@export
def curlcurl(sigma):
    A = np.eye(2)
    s = [np.asarray([[.5, 0], [0, .5]]), np.asarray([[.5, 0], [0, .5]])]
    C = Crystal(A, s)
    L = Operator(C)
    L.add_multiplier(np.asarray(
        [[2 + 2 / 3 * sigma, -1], [-1, 2 + 2 / 3 * sigma]]), np.array([0, 0]))
    L.add_multiplier(np.asarray(
        [[0, 0], [1, -1 + sigma / 6]]), np.array([-1, 0]))
    L.add_multiplier(np.asarray(
        [[0, 1], [0, -1 + sigma / 6]]), np.array([1, 0]))
    L.add_multiplier(np.asarray(
        [[-1 + sigma / 6, 1], [0, 0]]), np.array([0, -1]))
    L.add_multiplier(np.asarray(
        [[-1 + sigma / 6, 0], [1, 0]]), np.array([0, 1]))
    L.add_multiplier(np.asarray([[0, -1], [0, 0]]), np.array([1, -1]))
    L.add_multiplier(np.asarray([[0, 0], [-1, 0]]), np.array([-1, 1]))

    return L


@export
def laplace(dim=2):
    if dim == 1:
        A = np.eye(1)
        s = [np.asarray([[0]]), np.asarray([[0]])]
        C = Crystal(A, s)
        L = Operator(C)
    
        L.add_multiplier(np.eye(1) * -2, np.array([0]))
        L.add_multiplier(np.eye(1), np.array([1]))
        L.add_multiplier(np.eye(1), np.array([-1]))
    elif dim==2:
        A = np.eye(2)
        s = [np.asarray([[0, 0]]), np.asarray([[0, 0]])]
        C = Crystal(A, s)
        L = Operator(C)
    
        L.add_multiplier(np.eye(1) * -4, np.array([0, 0]))
        L.add_multiplier(np.eye(1), np.array([1, 0]))
        L.add_multiplier(np.eye(1), np.array([-1, 0]))
        L.add_multiplier(np.eye(1), np.array([0, 1]))
        L.add_multiplier(np.eye(1), np.array([0, -1]))
    else:
        print("Invalid dimension. dim has to be 1 or 2.")
    return L


@export
def fw_restriction(dim=2):
    if dim == 1:
        A = np.eye(1) * 2
        s = []
        s.append(np.asarray([[0], [1]]))  # domain
        s.append(np.asarray([[0]]))  # codomain
        C = Crystal(A, s)
        L = Operator(C)
        L.add_multiplier(np.asarray(
            [[1, 1 / 2]]), np.asarray([0]))
        L.add_multiplier(np.asarray(
            [[0, 1 / 2]]), np.asarray([-1]))
    elif dim == 2:
        A = np.eye(2) * 2
        s = []
        s.append(np.asarray([[0, 0], [0, 1], [1, 0], [1, 1]]))  # domain
        s.append(np.asarray([[0, 0]]))  # codomain
        C = Crystal(A, s)
        L = Operator(C)
        L.add_multiplier(np.asarray(
            [[1, 1 / 2, 1 / 2, 1 / 4]]), np.asarray([0, 0]))
        L.add_multiplier(np.asarray([[0, 0, 1 / 2, 1 / 4]]), np.asarray([-1, 0]))
        L.add_multiplier(np.asarray([[0, 1 / 2, 0, 1 / 4]]), np.asarray([0, -1]))
        L.add_multiplier(np.asarray([[0, 0, 0, 1 / 4]]), np.asarray([-1, -1]))
    else:
        print("Invalid dimension. dim has to be 1 or 2.")
    return L


@export
def graphene_tight_binding(t=None):
    if t is None:
        t = np.asarray([0, -1, 0, 0])

    if t is "3nn":
        t = np.asarray([-.36, -2.78, -.12, -.068])

    A = 1 / 2 * np.asarray([[3, 3], [np.sqrt(3), -np.sqrt(3)]])
    s = np.asarray([A@[1, 1] * 1 / 3, A@[1, 1] * 2 / 3])

    C = Crystal(A, [s, copy.deepcopy(s)])
    L = Operator(C)

    L.add_multiplier(np.asarray(
        [[t[0], t[1]], [t[1], t[0]]]), np.array([0, 0]))
    L.add_multiplier(np.asarray(
        [[t[2], t[1]], [0, t[2]]]), np.array([-1, 0]))
    L.add_multiplier(np.asarray(
        [[t[2], t[1]], [0, t[2]]]), np.array([0, -1]))
    L.add_multiplier(np.asarray(
        [[t[2], 0], [t[1], t[2]]]), np.array([1, 0]))
    L.add_multiplier(np.asarray(
        [[t[2], 0], [t[1], t[2]]]), np.array([0, 1]))

    L.add_multiplier(np.asarray(
        [[t[2], t[3]], [t[3], t[2]]]), np.array([1, -1]))
    L.add_multiplier(np.asarray(
        [[t[2], t[3]], [t[3], t[2]]]), np.array([-1, 1]))
    L.add_multiplier(np.asarray(
        [[0, 0], [t[3], 0]]), np.array([1, 1]))
    L.add_multiplier(np.asarray(
        [[0, t[3]], [0, 0]]), np.array([-1, -1]))

    return L


@export
def graphene_dirac_restriction(m=2, wl=.5, wlh=0):
    ws = wl + wlh - 1
    A = 2**(m - 2) / 2 * np.asarray([[3, 3], [np.sqrt(3), -np.sqrt(3)]])
    Ac = 2 * A

    s01 = [A@[1, 1] * 1 / 3, A@[1, 1] * 2 / 3]

    domain = np.asarray(
        [y + x for x in [A@[0, 0], A@[1, 0], A@[0, 1], A@[1, 1]] for y in s01])
    codomain = np.asarray([Ac@[1, 1] * 1 / 3, Ac@[1, 1] * 2 / 3])

    C = Crystal(Ac, [domain, codomain])

    L = Operator(C)

    m = np.zeros([2, 8])
    m[1, 0] = wl
    L.add_multiplier(m, np.array([1, 1]))

    m = np.zeros([2, 8])
    m[1, [0, 2, 4]] = [ws, wlh, ws]
    L.add_multiplier(m, np.array([1, 0]))

    m = np.zeros([2, 8])
    m[1, [0, 2, 4]] = [ws, ws, wlh]
    L.add_multiplier(m, np.array([0, 1]))

    m = np.zeros([2, 8])
    m[1, 4] = wl
    m[0, 5] = wl
    L.add_multiplier(m, np.array([1, -1]))

    m = np.zeros([2, 8])
    m[0, [1, 3, 5, 7]] = [1, ws, ws, wlh]
    m[1, [0, 2, 4, 6]] = [wlh, ws, ws, 1]
    L.add_multiplier(m, np.array([0, 0]))

    m = np.zeros([2, 8])
    m[1, 2] = wl
    m[0, 3] = wl
    L.add_multiplier(m, np.array([-1, 1]))

    m = np.zeros([2, 8])
    m[0, [3, 5, 7]] = [wlh, ws, ws]
    L.add_multiplier(m, np.array([0, -1]))

    m = np.zeros([2, 8])
    m[0, [3, 5, 7]] = [ws, wlh, ws]
    L.add_multiplier(m, np.array([-1, 0]))

    m = np.zeros([2, 8])
    m[0, 7] = wl
    L.add_multiplier(m, np.array([-1, -1]))

    return L
