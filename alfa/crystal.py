# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""


import numpy as np

from itertools import product
import matplotlib.pyplot as plt
import copy

from .lattice import Lattice


class Crystal(Lattice):
    """
    Create a Crystal; See Definition 2.3 of aLFA_Article.

    Parameters
    ----------
    A:  needs to be array_like as it will be casted to numpy.asarray(A).
        It further needs to be square and nonsingular.

    s:  Structure elements. List of N numpy.ndarrays of size 2 (s[j].ndim==2):
        N   = Number of Crystals > 0
        s[j].shape[0] = Number (>0) of points in crystal j in 0,...,N-1.
        s[j].shape[1] == dimensionality of the underlying lattice (self.dim)

        Further the coordinates must be unique, i.e., s[j][i] != s[j][k].

    """

    def __init__(self, A=np.eye(2), s=[np.asarray([[0, 0]])]):
        self.A = A
        self.s = s

    @property
    def s(self):
        """
        numpy.ndarray of size N x K x dim.
            Represents the structure elements of the crystal Lattice.
            (The actual Crystals is given by the points {A*z+s[j] :
             z in \\mathbb{Z}^{dim} })
        """
        return self._s

    @s.setter
    def s(self, s):
        assert isinstance(s, list), "structure element must be a list."
        assert len(s) > 0, "structure element must be of size > 0"
        for j in range(len(s)):
            assert isinstance(
                s[j], np.ndarray), "Each s[j] must be a numpy.ndarray."
            s[j] = np.asarray(s[j], dtype=np.float)
            assert s[j].ndim == 2, "s[j].ndim must be 2."
            assert s[j].shape[0] > 0, "Number of elements in crystal must be > 0."
            assert s[j].shape[1] == self.dim, "s[j].shape[1] must be equal to dim."
            assert np.unique(s[j], axis=0).shape[0] == s[j].shape[0],\
                "coordinates must be unique."
        self._s = s

    def _duplicate_domain(self):
        """
        Only used in the constructor of operator if C.s is of size 1.
        Then, the codomain C.s[1] is initialized as C.s[0] (= domain).
        """
        self._s.append(copy.deepcopy(self._s[0]))

    def plot_crystal_idx(self, xlim=None, idx=0, **kwargs):
        """
        Plots the crystals C.s[idx[0]], ... , C.s[idx[-1]] into
        current figure.
        """
        self.plot_lattice(xlim)
        
        ls = []
        for i in range(0, self.dim):
            ls.append(np.arange(xlim[2*i], xlim[2*i+1]+1, 1))

        integerpoints = np.asarray(list(product(*ls)))
        self._plot_crystal_points(integerpoints, idx, **kwargs)


    def plot_crystal(self, xlim=None, **kwargs):
        """
        Plots the crystals C.s[0], ... , C.s[-1] into len(C.s) subplots of the
        current figure.
        """
        if xlim is None:
            xlim = [-2, 2, -2, 2]
            
        if self.dim <= 2:
            fig = plt.gcf()
            ax = [fig.add_subplot(1, len(self.s), x + 1)
                  for x in range(len(self.s))]
            for idx, sidx in enumerate(self.s):
                plt.sca(ax[idx])
                self.plot_crystal_idx(xlim, idx, **kwargs)
                plt.axis('equal')
                
    def _plot_crystal_points(self, x, idx, **kwargs):
        """
        Plots the crystal points of C.s[idx] at C.A@x into current axis.
        """
        markerlist = kwargs.get('markerlist', ['o'])
        
        ax = plt.gca()
        
        if x.ndim == 1:
            x = np.asarray([x])
        Ax = x@self.A.T
        
        for k, se_k in enumerate(self.s[idx]):
            #Ax = self.A@x
            pos = Ax + se_k
            if self.dim == 1:
                ax.scatter(pos, 0*pos,
                           color=kwargs.get('color', 'k'),
                           edgecolor=kwargs.get('edgecolor', None),
                           alpha=kwargs.get('alpha', 1),
                           s=kwargs.get('s', 20),
                           zorder=kwargs.get('zorder', None),
                           marker=markerlist[k % len(markerlist)])
            elif self.dim == 2:
                ax.scatter(pos[:,0], pos[:,1],
                           color=kwargs.get('color', 'k'),
                           edgecolor=kwargs.get('edgecolor', None),
                           alpha=kwargs.get('alpha', 1),
                           s=kwargs.get('s', 20),
                           zorder=kwargs.get('zorder', None),
                           marker=markerlist[k % len(markerlist)])
