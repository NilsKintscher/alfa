# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""


import numpy as np
import scipy.linalg as linalg

import matplotlib.pyplot as plt
from alfa.functions import isintegral, smith_normal_form, hermite_normal_form


class Lattice(object):
    """
    Create a (Bravais) Lattice. Definition 2.1 of aLFA_Article.

    Parameters
    ----------
    A:  needs to be array_like as it will be casted to numpy.asarray(A).
        It further needs to be square and nonsingular.

    """

    def __init__(self, A=np.eye(2)):
        self.A = A

    @property
    def A(self):
        """
        numpy.ndarray of size dim x dim.
            Represents the Lattice basis in matrix format. A has to be non-
            singular.

            The actual Lattice is given by the points
                     {A*z : z integral array of size dim }
        """
        return self._A

    @A.setter
    def A(self, A):
        A = np.asarray(A, order='F', dtype=np.float)
        assert A.ndim == 2, "A.ndim is not equal to 2."
        assert A.shape[0] == A.shape[1], "A is not square."
        assert linalg.det(A) != 0, "A is not non-singular."
        self._A = A
        self._iA = linalg.inv(A)
        self._dA = self._iA.T

    @property
    def dim(self):
        """
        integer > 0
            Dimension of the Lattice
        """
        return self._A.shape[0]

    @property
    def iA(self):
        """
        numpy.ndarray of size dim x dim
            inverse of lattice basis A, i.e., A^-1
        """
        return self._iA

    @property
    def dA(self):
        """
        numpy.ndarray of size dim x dim
            Represents the dual Lattice basis of A defined by dA = inv(A)^T
        """
        return self._dA

    def plot_lattice(self, xlim=None, draw_primitive_vectors=True):
        """
        Plots the lattice into current axis plt.gca()

        Parameters
        ----------
        xlim:  [x0min, x0max, x1min, x1max], s.t.
            A[x0,x1] is plotted with x0 in [x0min, x0max], x1 in [x1min,x1max]

        """
        
        if self.dim == 1:
            if xlim is None:
                xlim = [-4, 3]
            
            ax = plt.gca()
            x = (self.A*np.arange(xlim[0], xlim[1]+2,1))[0]
            y = 0*x
            ax.plot(x, y, color='k', marker="|", markersize=15,
                        linestyle=':', linewidth=.5)
            ax.axes.get_yaxis().set_ticks([])

        elif self.dim == 2:
            if xlim is None:
                xlim = [-4, 3, -4, 3]

            ax = plt.gca()
            
            for i in range(np.int(xlim[0]), np.int(xlim[1]) + 2, 1):
                xy = np.asarray([[i, i], [xlim[2], xlim[3] + 1]])
                A0xy = self.A@xy
                ax.plot(A0xy[0, :], A0xy[1, :], color='k',
                        linestyle=':', linewidth=.5)
            for i in range(np.int(xlim[2]), np.int(xlim[3]) + 2, 1):
                xy = np.flipud(np.asarray([[i, i], [xlim[0], xlim[1] + 1]]))
                A0xy = self.A@xy
                ax.plot(A0xy[0, :], A0xy[1, :], color='k',
                        linestyle=':', linewidth=.5)

            if draw_primitive_vectors:
                ax.annotate(
                    "",
                    xy=(0, 0), xycoords='data',
                    xytext=self.A[:, 0], textcoords='data',
                    va="center",
                    ha="center",
                    arrowprops=dict(arrowstyle="<|-",
                                    color="0.1",
                                    shrinkA=0, shrinkB=0,
                                    ),
                    zorder=0
                )

                ax.annotate(
                    "",
                    xy=(0, 0), xycoords='data',
                    xytext=self.A[:, 1], textcoords='data',
                    va="center",
                    ha="center",
                    arrowprops=dict(arrowstyle="<|-",
                                    color="0.1",
                                    shrinkA=0, shrinkB=0,
                                    ),
                    zorder=0
                )
            else:
                print("Lattice plot only implemented in the 1D and 2D case.")

    def lcm(self, other, rmax=10):
        """
        Computes a least common multiple lattice of two lattices.
        Algorithm B.7 of aLFA_Article.
        """

        M0 = linalg.solve(self.A, other.A)
        for r in range(1, rmax + 1):
            M = r * M0
            isint, M_int = isintegral(M)
            if isint:
                break

        assert isint,\
            "Least common multiple lattice not found in given range"

        S, U, V = smith_normal_form(M_int)  # S = UMV
        N = np.diag(r / np.gcd(np.diag(S), r))
        C = other.A@V@N
        return Lattice(C)

    def elements_in_quotientspace(self, other):
        """
        Computes the elements in the lattice torus  (self / other).
        Algorithm B.4 of aLFA_Article.
        """
        M = linalg.solve(self.A, other.A)
        isint, M_int = isintegral(M)
        assert isint, "other is not a sublattice of self."
        H = hermite_normal_form(M_int)
        dh = H.diagonal()
        s = np.zeros([dh.prod(), self.dim])
        for i in range(1, s.shape[0] + 1):
            k = i - 1
            for j in range(0, self.dim):
                t = k % dh[j]
                k = (k - t) / dh[j]
                s[i - 1] += t * self.A[:, j]  # This is in self.A
        return s
