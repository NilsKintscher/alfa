# distutils: language = c++
# cython: language_level=3
# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""
#from __future__ import division


#cimport c_functions 


import cython

import numpy as np
cimport numpy as np


DTYPEf = np.float64
DTYPEi = np.int_
DTYPEc = np.complex128
#ctypedef np.float64_t DTYPEf_t
#ctypedef np.int_t DTYPEi_t
#ctypedef np.complex128_t DTYPEc_t

cdef DTYPEf_t rel_tol = 1e-05
cdef DTYPEf_t abs_tol = 1e-08


cdef bool_t  _is_close(DTYPEf_t a, DTYPEf_t b):
    """
    check if two numbers are equal neglecting numerical inexactness:
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.isclose.html
    """
    cdef DTYPEf_t amb = fabs(a - b)
    if amb <= rel_tol * abs_tol or amb <= rel_tol * max(fabs(a), fabs(b)):
        return True
    return False


cdef void _find_permutation_and_shifts(int s_max, 
                                       int dim,
                                       np.ndarray[DTYPEf_t, ndim=2] iAs_old, 
                                       np.ndarray[DTYPEf_t, ndim=2] iAs_new, 
                                       DTYPEi_t[:] perm_p,
                                       DTYPEi_t[:,:] e):
    cdef bool_t isint
    cdef int i, j, k, l
    
    cdef np.ndarray[DTYPEf_t, ndim = 1] iAshift
    cdef np.ndarray[DTYPEf_t, ndim= 1] rounded
    
    iAshift = np.empty([dim], dtype=DTYPEf)
    rounded = np.empty([dim], dtype=DTYPEf)

    for i in range(s_max):
        for j in range(s_max):
            isint = True
            for k in range(dim):
                iAshift[k] = (-iAs_new[i, k] + iAs_old[j, k])
                rounded[k] = round(iAshift[k])
                if(not _is_close(iAshift[k], rounded[k])):
                    isint = False
                    break

            if isint:
                assert(perm_p[j] == -
                       1), """permutation entry already set.
                Structure element not compatible."""
                perm_p[j] = i
                for k in range(dim):
                    e[j, k] = <DTYPEi_t > rounded[k]
                break
        else:
            print("iAs_new[i] :")
            print(iAs_new[i])
            print("not found in old structurelement iAs_old: ")
            print(iAs_old)
            assert(False), "Structure elements are not compatible."


def find_permutation_and_shifts(L, s_old, s_new):
    """
    Finds the permutation and shifts for two congruent structure elements with
    respect to a underlying translational invariance.
    This function is for example used in the function
    operator.change_structure_elements

    L: alfa.Lattice
    """
    
    cdef np.ndarray[DTYPEf_t, ndim= 2] iAt = L.iA.T
    
    cdef np.ndarray[DTYPEf_t, ndim= 2] iAs_old, iAs_new
    iAs_old = s_old@iAt
    iAs_new = s_new@iAt

    cdef int s_max = s_new.shape[0]
    cdef int dim = L.dim
    
    #init permutation
    cdef DTYPEi_t[:] perm_p = -1 * np.ones([s_max], DTYPEi)
    #init shift
    cdef DTYPEi_t[:, :] e  
    e = np.empty([s_max, dim], dtype=DTYPEi)
    
    _find_permutation_and_shifts(s_max, 
                                 dim,
                                 iAs_old, 
                                 iAs_new, 
                                 perm_p,
                                 e)
    
    return perm_p, e
        