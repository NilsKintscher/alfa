    # distutils: language = c++
# cython: language_level=3
# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division

import cython

import numpy as np
cimport numpy as np

# https://flothesof.github.io/cython-complex-numbers-parallel-optimization.html
DTYPEf = np.float64
DTYPEi = np.int_
DTYPEc = np.complex128
ctypedef np.float64_t DTYPEf_t
ctypedef np.int_t DTYPEi_t
ctypedef np.complex128_t DTYPEc_t


from libc.math cimport fabs
from libc.math cimport round, floor
from libcpp cimport bool as bool_t

from alfa.crystal import Crystal


#cimport alfa.c_functions
from alfa.c_functions cimport _is_close
from alfa.c_functions import find_permutation_and_shifts

from alfa.functions import isclose, isintegral



import scipy.linalg as linalg

import warnings
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable

import time





@cython.boundscheck(False)
@cython.wraparound(False)
cdef assign_block(DTYPEc_t[:, :] m,
                  DTYPEc_t[:, :] block,
                  DTYPEi_t row_start, DTYPEi_t row_end,
                  DTYPEi_t col_start, DTYPEi_t col_end):
    """
    Used within the function Operator.wrt_lattice() to assign a subblock of a
    matrix multiplier.
    """
    cdef DTYPEi_t i, j
    for i in range(row_end - row_start):
        for j in range(col_end - col_start):
            m[i + row_start, j + col_start] = block[i, j]
            # mm[i * brs:(i + 1) * brs, j * bcs:(j + 1) *
            #   bcs] = self._m[k].matrix


class Multiplier(object):
    """
    Represents a multiplier used in class Operator.  See Theorem 3.1 of aLFA_Article.

    matrix: matrix of size C.s[1].shape[0] x C.s[0].shape[0]

    pos (integral) position y of multiplier (fractional coordinate).

    """

    def __init__(self, matrix, pos):
        self.matrix = np.asarray(matrix, dtype=DTYPEc)
        self.pos = pos


class Operator(object):
    """
    A multiplication operator C:T_{A}^s[0] -> T_{A}^s[1]
    with (Cf)(x) = sum_y m(y)f(x+Ay);
    See Theorem 3.1 of aLFA_Article.
    
    Parameters
    ----------
    C:
        Underlying alfa.Crystal with
        C.A = underlying lattice,
        C.s[0] = structure element of the domain
        C.s[1] = structure element of the co-domain

    m:
        List of Multiplier with
        m[i].matrix  of size C.s[1].shape[0] x C.s[0].shape[0]
        m[i].pos (integral) position y of multiplier (fractional coordinate)
        C.A*m[i].pos corresponds to the cartesian coordinate.

    _compatibility_check_only:
        if true: computation rules (multiplication, addition,...)
        is replaced with a simple compatibility check regarding domain and codomain
    """

    def __init__(self, C):

        # The following procudes an error if "import alfa" is called twice:
        # assert isinstance(C, Crystal), "C must be of type Crystal" # todo:
        # find a fix....
        if len(C.s) == 1:
            print("Implicitly setting codomain to domain.")
            C._duplicate_domain()
        assert len(C.s) == 2,\
            "C must define a domain and codomain structure element"
        self._C = C
        self._m = []

        # Replace computation rules with a compatibility check:
        self._compatibility_check_only = False

    @property
    def C(self):
        """
        alfa.Crystal
        """
        return self._C

    @C.setter
    def C(self, C):
        print("Not yet implemented.")

    @property
    def m(self):
        return self._m

    def all_pos(self):
        return np.asarray([m.pos for m in self._m], dtype=DTYPEi)

    def add_multiplier(self, m, y, plus=False):
        """
        Adds a multiplier to the (sorted) multiplier List of the operator.

        Parameters
        ----------
        m multiplier of size C.s[1].ndim x C.s[0].ndim
        y (integral) position y of multiplier (fractional coordinate)
        plus :    False: replace multiplier if already existent.
                  True: add to multiplier if already existent.
        """
        y = np.asarray(y)
        is_int, y = isintegral(y)
        assert is_int, "non integer coordinate detected."
        assert y.ndim == 1, "y.ndim needs to be 1."
        assert y.shape[0] == self.C.dim, \
            "dimension of the crystal and position y must agree"

        #m = np.asarray(m)
        assert m.ndim == 2, "dimension mismatch. m.ndim must be 2. (matrix)"
        assert m.shape[0] == len(self.C.s[1]), """dimension mismatch. \
            m.shape[0] must correspond to the structure element s of the
            codomain"""
        assert m.shape[1] == len(self.C.s[0]), """dimension mismatch. \
            m.shape[1] must correspond to the structure element s of the
            domain"""

        # list is lexicographically sorted with respect to the operator <= of
        # lists.

        # inserting element....
        i = 0
        while i < len(self._m):
            if list(self._m[i].pos) < list(y):
                i += 1
            else:
                if list(self._m[i].pos) == list(y):
                    if not plus:
                        print("Multiplier position already defined. Replacing")
                        del self._m[i]
                    else:
                        # add to multiplier
                        self._m[i].matrix += m
                        return
                break

        self._m.insert(
            i,
            Multiplier(
                m,
                np.asarray(
                    y,
                    dtype=DTYPEi)
            ))

    def del_zero_multiplier(self):
        """
        deletes zero multipliers.
        """
        self._m = [x for x in self._m if x.matrix.any()]

    def del_multiplier(self, y):
        """
        delete multiplier at position y
        """
        y = np.asarray(y)
        is_int, y = isintegral(y)
        assert is_int, "non integer coordinate detected."
        assert y.ndim == 1, "x.ndim needs to be 1."
        assert y.shape[0] == self.C.dim, \
            "dimension of the crystal and position y must agree"

        i = 0
        for i in range(0, len(self._m)):
            if list(self._m[i].pos) == list(y):
                del self._m[i]
                return

        print("multiplier position not found")

    def print_multiplier(self):
        """
        Prints the multiplier to the console
        """
        for m in self._m:
            print(m.matrix, ", ", m.pos)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def wrt_lattice(self, L):
        """
        Rewrites the operator with respect to the Lattice L
        (Corresponds to Algorithm B.4 of aLFA_Article.)
        """

        cdef DTYPEi_t i, j, k, l, yit, brs, bcs
        cdef DTYPEf_t rounded
        cdef bool_t isc_all
        cdef DTYPEf_t[:] yt
        cdef DTYPEi_t[:, :] y_old, y_new,
        cdef np.ndarray[DTYPEf_t, ndim = 2] At, iA
        cdef np.ndarray[DTYPEc_t, ndim = 2] mm
        cdef DTYPEf_t[:, :] Ay_old, Ay_new
        cdef DTYPEf_t[:, :] e = self.C.elements_in_quotientspace(L)
        cdef np.ndarray[DTYPEf_t, ndim= 2] s0 = self.C.s[0]
        cdef np.ndarray[DTYPEf_t, ndim= 2] s1 = self.C.s[1]
        cdef np.ndarray[DTYPEf_t, ndim = 2] u0, u1
        cdef int dim = self.C.dim
        u0 = np.tile(e, (1, s0.shape[0])).reshape(-1, dim) \
            + np.tile(s0, (e.shape[0], 1)).reshape(-1, dim)
        u1 = np.tile(e, (1, s1.shape[0])).reshape(-1, dim) \
            + np.tile(s1, (e.shape[0], 1)).reshape(-1, dim)

        # initialize new crystal operator.
        op = Operator(Crystal(L.A, [u0, u1]))

        # find positions of new multiplier
        if self._compatibility_check_only:
            op._compatibility_check_only = True
        else:
            # positions of old multiplier
            y_old = self.all_pos()
            Ay_old = y_old@self.C.A.T
            iA = L.iA
            At = L.A.T
            if y_old.size > 0:
                # Find all combinations of floor(C\(A*y+e[i]-e[j])):

                # Due to numerical inexactness, we cannot use the following
                #
                # y_new = np.unique(np.asarray([np.floor(
                #    L.iA@(y + f - g)
                # ) for y in Ay_old for f in e for g in e], dtype=DTYPEi), axis=0)
                #
                # Instead, we have to check if (L.iA@(...)) is close to round(L.iA@(...)) before
                # using floor(L.iA@(...)).

                y_new = np.zeros(
                    [Ay_old.shape[0] * e.shape[0] * e.shape[0], Ay_old.shape[1]], dtype=DTYPEi)
                yt = np.empty(e.shape[1], dtype=DTYPEf)
                yit = 0

                for i in range(Ay_old.shape[0]):
                    for j in range(e.shape[0]):
                        for k in range(e.shape[0]):
                            for l in range(e.shape[1]):
                                yt[l] = Ay_old[i, l] + e[j, l] - e[k, l]
                            yt = iA@yt

                            for l in range(yt.shape[0]):
                                rounded = round(yt[l])
                                if _is_close(yt[l], rounded):
                                    y_new[yit, l] = <DTYPEi_t > (rounded)
                                else:
                                    y_new[yit, l] = <DTYPEi_t > floor(yt[l])
                            yit += 1
                y_new = np.unique(y_new, axis=0)
                # create block matrices.
                brs = self.C.s[1].shape[0]  # block row size
                bcs = self.C.s[0].shape[0]  # block column size
                Ay_new = y_new@At  # new positions...
                for yit in range(len(y_new)):

                    mm = np.zeros([u1.shape[0], u0.shape[0]], dtype=DTYPEc)

                    # is the blockmultiplier mm[i,j] nonzero?

                    for i in range(e.shape[0]):
                        for j in range(e.shape[0]):
                            for l in range(e.shape[1]):
                                yt[l] = Ay_new[yit, l] - e[i, l] + e[j, l]
                            #yt = Ay_new[yit] - e[i] + e[j]
                            for k in range(Ay_old.shape[0]):
                                isclose_all = True
                                for l in range(Ay_old.shape[1]):
                                    if not _is_close(yt[l], Ay_old[k, l]):
                                        isclose_all = False
                                        break
                                if isclose_all:
                                    matblock = self._m[k].matrix
                                    assign_block(mm, matblock,
                                                 i * brs, (i + 1) * brs,
                                                 j * bcs, (j + 1) * bcs)
                                    break

                    mm_nparr = np.asarray(mm)
                    if mm_nparr.any():
                        op.add_multiplier(mm_nparr, y_new[yit])

        return op

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def change_structure_elements(self, s):
        """
        Corresponds to Algorithm B.6 of aLFA_Article.
        s:  Structure elements. List of N= numpy.ndarrays of size s[k].ndim=2:
            s[0] (s[1]) corresponds to domain (codomain)
            s[k].shape[0] = Number (>0) of points in crystal, k in 0,1
            s[k].shape[1] == self.C.dim
            s[0] = domain; must be consistent with self.C.s[0] w.r.t self.C.A
            s[1] = codomain; must be consistent with self.C.s[1] w.r.t self.C.A
        """
        cdef np.ndarray[DTYPEf_t, ndim= 2] s0_new, s1_new
        s0_new = s[0]
        s1_new = s[1]
        cdef np.ndarray[DTYPEf_t, ndim= 2] iAt = self.C.iA.T
        cdef int dim = self.C.dim # iAt.shape[0]

        cdef np.ndarray[DTYPEf_t, ndim= 2] s0_old = self.C.s[0]
        cdef np.ndarray[DTYPEf_t, ndim= 2] s1_old = self.C.s[1]

        
        
#        cdef np.ndarray[DTYPEf_t, ndim= 2] iAs0_old = s0_old@iAt
#        cdef np.ndarray[DTYPEf_t, ndim= 2] iAs1_old = s1_old@iAt
#        cdef np.ndarray[DTYPEf_t, ndim= 2] iAs0_new = s0_new@iAt
#        cdef np.ndarray[DTYPEf_t, ndim= 2] iAs1_new = s1_new@iAt




        cdef int s0_max = s0_new.shape[0]
        cdef int s1_max = s1_new.shape[0]

        # init Permutation matrices
        cdef DTYPEi_t[:] perm_p = -1 * np.ones([s0_max], DTYPEi)
        cdef DTYPEi_t[:] perm_s = -1 * np.ones([s1_max], DTYPEi)
        cdef DTYPEi_t[:, :] e, f

        cdef DTYPEi_t[:, :] ue, uf
        cdef DTYPEi_t[:] ue_ind, uf_ind
        cdef DTYPEi_t[:, :, :, :] yv_new
        cdef DTYPEi_t[:, :] yv_old
        cdef DTYPEi_t[:] y_tmp
        cdef int i, j, k, l

        cdef bool_t init_new
        cdef bool_t isint
        cdef bool_t is_zero

        cdef DTYPEi_t[:] si, sj, sk

        cdef DTYPEc_t[:, :] mm, mat

        op = Operator(
            Crystal(
                copy.deepcopy(
                    self.C.A), [
                    s0_new, s1_new]))
        if len(self.m) > 0:
            #_find_permutation_and_shifts(s0_max, dim, iAs0_old, iAs0_new, perm_p, e)
            perm_p, e = find_permutation_and_shifts(self.C, s0_old, s0_new)
            perm_s, f = find_permutation_and_shifts(self.C, s1_old, s1_new)

            ue, ue_ind = np.unique(e, return_inverse=True, axis=0)
            ue_ind_list = [np.argwhere(
                np.equal(e, x).all(axis=1)).reshape(-1) for x in ue]

            uf, uf_ind = np.unique(f, return_inverse=True, axis=0)
            uf_ind_list = [np.argwhere(
                np.equal(f, x).all(axis=1)).reshape(-1) for x in uf]

            # Find positions of new multiplier.
            yv_old = self.all_pos()  # old positions.

            yv_new = np.empty(
                (yv_old.shape[0],
                 uf.shape[0],
                 ue.shape[0],
                 dim),
                dtype=DTYPEi)

            for k in range(yv_old.shape[0]):
                for i in range(uf.shape[0]):
                    for j in range(ue.shape[0]):
                        for l in range(dim):
                            yv_new[k, i, j, l] = yv_old[k, l] \
                                + ue[j, l] - uf[i, l]

            sk, si, sj = np.unravel_index(
                np.lexsort(np.flipud(np.asarray(
                    yv_new, dtype=DTYPEi).reshape(-1, dim).T)),
                (yv_new.shape[0], yv_new.shape[1], yv_new.shape[2]))

            init_new = True

            l = 0
            first = True
            mat = self._m[sk[0]].matrix

            while l < si.shape[0]:
                if init_new:
                    y_tmp = yv_new[sk[l], si[l], sj[l]]

                    mm = np.zeros([s1_max, s0_max], dtype=DTYPEc)
                    is_zero = True
                    init_new = False

                ue_ind = ue_ind_list[sj[l]]
                uf_ind = uf_ind_list[si[l]]

                for j in range(ue_ind.shape[0]):
                    for i in range(uf_ind.shape[0]):
                        if np.abs(mat[uf_ind[i], ue_ind[j]]) > 0:
                            is_zero = False
                            mm[perm_s[uf_ind[i]], perm_p[ue_ind[j]]
                               ] = mat[uf_ind[i], ue_ind[j]]

                l = l + 1
                if l == si.shape[0] or not np.array_equal(
                        y_tmp, yv_new[sk[l], si[l], sj[l]]):
                    # add multiplier.
                    if not is_zero:
                        op.add_multiplier(mm, y_tmp)
                    init_new = True

                if l < si.shape[0] and sk[l] != sk[l - 1]:
                    mat = self._m[sk[l]].matrix

        return op

    def normalize(self):
        """
        Corresponds to Algorithm B.2 of aLFA_Article.
        Normalizes the structure elements, i.e., s[k], k=1,2, fulfills:
            - s[k][i] is found in A[0,1)^dim
            - s[k] is sorted lexicographically:
                s[k][i] < s[k][j]
                <=> s[k][i][0] < s[k][j][0]
                    or (s[k][i][0] == s[k][j][0] and s[k][i][1] < s[k][j][1])
                    or (s[k][i][1] == s[k][j][1] and s[k][i][2] < s[k][j][2])
                    ....
        """

        cdef DTYPEi_t j, k
        cdef DTYPEf_t pos, pos_rounded
        cdef np.ndarray[DTYPEf_t, ndim= 1] y
        cdef np.ndarray[DTYPEf_t, ndim= 2] s0, s1, Cs0, Cs1
        cdef np.ndarray[DTYPEf_t, ndim= 2] iA = self.C.iA
        cdef np.ndarray[DTYPEf_t, ndim= 2] A = self.C.A
        Cs0 = self.C.s[0]
        Cs1 = self.C.s[1]
        s0 = np.empty(self.C.s[0].shape, dtype=DTYPEf)
        s1 = np.empty(self.C.s[1].shape, dtype=DTYPEf)
        for j in range(Cs0.shape[0]):
            y = iA@Cs0[j]
            for k in range(len(y)):
                pos = y[k]
                pos_rounded = round(pos)
                if _is_close(pos, pos_rounded):
                    y[k] = pos_rounded
                else:
                    y[k] = np.floor(pos)
            s0[j] = Cs0[j] - A@y

        for j in range(Cs1.shape[0]):
            y = iA@Cs1[j]
            for k in range(len(y)):
                pos = y[k]
                pos_rounded = round(pos)
                if _is_close(pos, pos_rounded):
                    y[k] = pos_rounded
                else:
                    y[k] = np.floor(pos)
            s1[j] = Cs1[j] - A@y

        # sort s[i] lexicographically:
        # need to flipud s.t. lexsort does the same as list comparism.

        s = [s0[np.lexsort(np.flipud(s0.transpose()))],
             s1[np.lexsort(np.flipud(s1.transpose()))]]

        op = self.change_structure_elements(s)
        self._m = op._m
        self._C = op._C

    def make_operators_compatible(self, other):
        """
        Rewrites both operator w.r.t a single lattice and normalizes the
        structure elements.
        """
        L = self.C.lcm(other.C)
        a = self.wrt_lattice(L)
        a.normalize()
        b = other.wrt_lattice(L)
        b.normalize()
        return a, b

    def __a_plus_b_times_s__(self, other, s):
        """
        computes self + other*s with s = scalar.
        """
        if self._compatibility_check_only:
            # Create an empty operator with correct domain and codomain.
            assert isclose(self.C.s[0], other.C.s[0])\
                and isclose(self.C.s[1], other.C.s[1]),\
                "Structure elements must be identical."
            ab = Operator(Crystal(self.C.A, self.C.s))
            ab._compatibility_check_only = True
            return ab
        else:
            a, b = self.make_operators_compatible(other)
            assert isclose(a.C.s[0], b.C.s[0])\
                and isclose(a.C.s[1], b.C.s[1]),\
                "Structure elements must be identical."
            ab = copy.deepcopy(a)
            for m in b._m:
                ab.add_multiplier(s * m.matrix, m.pos, True)
            ab.del_zero_multiplier()
            return ab

    def __add__(self, other):
        """
        Computes A+B for A, B being alfa.Operator
        """
        return self.__a_plus_b_times_s__(other, 1)

    def __sub__(self, other):
        """
        Computes A-B for A, B being alfa.Operator
        """
        return self.__a_plus_b_times_s__(other, -1)

    def __mul__(self, other):
        """
        Computes O*s for a scalar s and alfa.Operator O
        """
        if self._compatibility_check_only:
            ab = Operator(Crystal(self.C.A, self.C.s))
            ab._compatibility_check_only = True
            return ab
        else:
            a = copy.deepcopy(self)
            for m in a._m:
                m.matrix = other * m.matrix
            return a

    def __rmul__(self, other):
        """
        Computes s*O for a scalar s and alfa.Operator O
        """
        return self.__mul__(other)

    def __matmul__(self, other):
        """
        Computes A@B for A, B being alfa.Operator
        """
        if self._compatibility_check_only:
            # Create an empty operator with correct domain and codomain.
            assert isclose(self.C.s[0], other.C.s[1]), """Structure
            elements self.C.s[0] & other.C.s[1] munp.asarray(symst be identical."""
            s = [other.C.s[0], self.C.s[1]]
            ab = Operator(Crystal(self.C.A, s))
            ab._compatibility_check_only = True
            return ab
        else:
            a, b = self.make_operators_compatible(other)
            assert isclose(a.C.s[0], b.C.s[1]),\
                "Structure elements a.C.s[0] & b.C.s[1] must be identical."
            s = [b.C.s[0], a.C.s[1]]

            ab = Operator(Crystal(a.C.A, s))
            for i in range(len(a._m)):
                for j in range(len(b._m)):
                    y = a._m[i].pos + b._m[j].pos
                    ab.add_multiplier(a._m[i].matrix@b._m[j].matrix, y, True)
            ab.del_zero_multiplier()
            return ab

    def transpose(self):
        """
        Computes the transpose of the operator.
        """
        if self._compatibility_check_only:
            op = Operator(Crystal(self.C.A, [self.C._s[1], self.C._s[0]]))
            op._compatibility_check_only = True
            return op
        else:
            op = copy.deepcopy(self)
            op.C._s = [op.C._s[1], op.C._s[0]]
            for m in op._m:
                m.matrix = m.matrix.T
                m.pos = -m.pos

            # sort
            y = np.asarray([m.pos for m in op._m])
            ind = np.lexsort(np.flipud(y.T))
            op._m = [op._m[i] for i in ind]
            return op

    def symbol(self, k, mat=None):
        """
        Computes the symbol of the operator w.r.t. to the wavevector k.
        """

        cdef np.ndarray[DTYPEc_t, ndim = 2] sym
        if(mat is None):
            sym = np.zeros(
                [self._C.s[1].shape[0], self._C.s[0].shape[0]], dtype=DTYPEc)
        else:
            sym = mat

        for m in self._m:
            sym += m.matrix * np.exp(1j * 2 * np.pi * k.dot(self.C.A@m.pos))
        return sym

    def plot(self, indfrom=None, indto=None, threshold=1e-15):
        """
        Plots the stencil of the operator into the current axis. The coloring
        corresponds to the real part of the matrix entries.

        Parameters
        ----------
        indfrom:  plots only arrows from domain C.s[0][i] with i in indfrom
        indto: plots only arrows to codomain C.s[1][i] with i in indto
        threshold: ignores matrix entries a with abs(a) < threshold
        """
        
        if self.C.dim == 2 or self.C.dim == 1:
            
            # some options...
            markersize_codomain = 200
            markersize_domain = 100
            markersize_codomain_self = markersize_codomain
            
            color_domain = 'salmon'
            
            # define colorbar axis.
            ax = plt.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            plt.sca(ax)
            if self.C.dim == 2:
                plt.axis('equal')
            
            yy = self.all_pos()
            
            if yy.size == 0:
                print("operator is 0. Plotting domain and codomain at [0,0]")
                self.C._plot_crystal_points(
                        np.zeros(self.C.dim), 1, markerlist=['h'], s=markersize_codomain_self, color="None", edgecolor = 'k', zorder=5)
                
                #self.C._plot_crystal_points(np.zeros(self.C.dim),
                #                            0, markerlist=['2'], s=markersize_domain, color=color_domain, zorder=5)
                
                self.C.plot_crystal_idx(
                        np.zeros(self.C.dim*2), 0, markerlist=['2'], s=markersize_domain, color=color_domain, zorder=5)
                return

            # plot the codomain at the lattice point [0, 0]
            self.C._plot_crystal_points(
                np.zeros(self.C.dim), 1, markerlist=['h'], s=markersize_codomain_self, color="None", edgecolor = 'k', zorder=5)

            # plot the relevant part of the domain.
            minmax = [y(yy[:,x]) for x in range(self.C.dim)  for y in [lambda z: np.min(z), lambda z: np.max(z)]  ]
            self.C.plot_crystal_idx(
                minmax, 0, markerlist=['2'], s=markersize_domain, color=color_domain, zorder=5)
            

            # filter columns/rows if necessarry...
            if indfrom is not None:
                indfrom = np.intersect1d(np.asarray(
                    range(self.m[0].matrix.shape[1])), np.asarray(indfrom))
            else:
                indfrom = np.asarray(range(self.m[0].matrix.shape[1]))

            if indto is not None:
                indto = np.intersect1d(np.asarray(
                    range(self.m[0].matrix.shape[0])), np.asarray(indto))
            else:
                indto = np.asarray(range(self.m[0].matrix.shape[0]))

            # First: Find all different values unique_vals, min and max in the
            # real part of all matrices
            unique_vals = []
            vmin = np.inf
            vmax = -np.inf
            for m in self.m:
                vmin = min(np.min(np.real(m.matrix[indto,:][:,indfrom])), vmin)
                vmax = max(np.max(np.real(m.matrix[indto,:][:,indfrom])), vmax)

                unique_vals += list(np.real(m.matrix[indto,:][:,indfrom]).reshape(1, -1)[0, :])
                unique_vals = list(set(unique_vals))

            unique_vals = np.asarray(unique_vals)
            # ignore entries with abs(x) < threshold.
            unique_vals = unique_vals[np.where(
                np.abs(unique_vals) > threshold)]
            cbar_ticks = [str(np.round(x, 2)) for x in unique_vals]
            # scale colormap 1 = max, 0 = min
            cbar_cmap = mpl.cm.viridis 
            cbar_norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

            cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cbar_cmap,
                                            orientation='vertical',
                                            norm=cbar_norm,
                                            ticks=unique_vals)
            cb1.set_ticklabels(cbar_ticks)


            

            # print arrows from domain to codomain.
            for m in self.m:
                coord_cartesian = self.C.A@m.pos
                # [i, j] =  [to (codomain), from (domain)] = [s[1], s[0]]
                for i in indto:
                    for j in indfrom:
                        # for i, j in np.ndindex(m.matrix.shape):
                        if(np.abs(m.matrix[i, j]) > threshold):
                            color = cbar_cmap(
                                cbar_norm(np.real(m.matrix[i, j])))
                            if linalg.norm(
                                    m.pos) == 0 and isclose(
                                    self.C.s[1][i],
                                    self.C.s[0][j]):
                                if self.C.dim == 1:
                                    ax.scatter(
                                        self.C.s[1][i][0],
                                        self.C.s[1][i][0]*0,
                                        color=color,
                                        marker='h',
                                        s=markersize_codomain_self,
                                        zorder=2)
                                else:
                                    ax.scatter(
                                        self.C.s[1][i][0],
                                        self.C.s[1][i][1],
                                        marker='h',
                                        color=color,
                                        s=markersize_codomain_self,
                                        zorder=2)
                            else:
                                if self.C.dim == 1:
                                    ax.annotate(
                                        "",
                                        xy=np.asarray([self.C.s[1][i],0]),
                                        xycoords='data',
                                        xytext=np.asarray([coord_cartesian
                                        + self.C.s[0][j],0]),
                                        textcoords='data',
                                        arrowprops=dict(
                                            arrowstyle="-|>",
                                            color=color,
                                            shrinkA=8,
                                            shrinkB=8,
                                            connectionstyle="arc3,rad=0.3"),
                                    )
                                else:
                                    ax.annotate(
                                        "",
                                        xy=self.C.s[1][i],
                                        xycoords='data',
                                        xytext=coord_cartesian
                                        + self.C.s[0][j],
                                        textcoords='data',
                                        arrowprops=dict(
                                            arrowstyle="-|>",
                                            color=color,
                                            shrinkA=8,
                                            shrinkB=8,
                                            connectionstyle="arc3,rad=0.3"),
                                    )
