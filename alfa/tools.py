# -*- coding: utf-8 -*-
"""
aLFA - automated local Fourier analysis
Copyright (C) 2019  Nils Kintscher, Karsten Kahl

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
from scipy import linalg
import copy

from alfa.crystal import Crystal
from alfa.operator import Operator

from alfa.export_decorator import export


@export
def identity(C):
    """
    Returns the identity operator on the domain/codomain C.
    """
    assert isinstance(
        C, Crystal), "argument of identity must be of type alfa.Crystal."
    L = Operator(C)
    assert L.C.s[0].shape[0] == L.C.s[1].shape[0], "unable to create identity"\
        " due to s[0].shape[0] != s[1].shape[0]"
    m = np.eye(L.C.s[0].shape[0]) 
    pos = np.zeros(L.C.dim)
    L.add_multiplier(m, pos)
    return L


@export
def zero(C):
    """
    Returns the 0 operator on the domain/codomain C.
    """
    assert isinstance(
        C, Crystal), "argument of identity must be of type alfa.Crystal."
    L = Operator(C)
    m = np.zeros([L.C.s[1].shape[0], L.C.s[0].shape[0]])
    pos = np.zeros(L.C.dim)
    L.add_multiplier(m, pos)
    return L


@export
def restriction_wrt(C, idx):
    """
    Returns the restriction operator R w.r.t. the index list idx, i.e.,
    (Rf)(x) = m*f(x) with

    m_ij = 1 iff i,j in idx,
    m_ij = 0 iff i or j notin idx.
    """
    assert isinstance(
        C, Crystal), "argument of identity must be of type alfa.Crystal."
    s0 = C.s[0]
    s1 = s0[idx]
    s = [s0, s1]
    Cnew = Crystal(C.A, s)
    L = Operator(Cnew)
    m = np.zeros([s1.shape[0], s0.shape[0]])
    for i, j in enumerate(idx):
        m[i, j] = 1
    L.add_multiplier(m, np.zeros(C.dim))
    return L


@export
def lower_triangle_of(L, idx=None, perm=None, omega=1.0):
    """
    Returns the operator G with
    the multipliers

    m_G^{(y)} = 1/omega*p@m_L^{(y)}@p if y = 0, (omega is the relaxation parameter)
    m_G^{(y)} = p@m_L^{(y)}@p if y < 0,
    m_G^{(y)} = 0 if y > 0,

    with P[i,j] = 1 if i=j in idx,
                = 0 else.
                
    
    """

    assert isinstance(L, Operator), " "
    G = Operator(L.C)
    P = np.zeros([G.C.s[1].shape[0], G.C.s[0].shape[0]])
    if idx is not None:
        idx = list(set(idx))
        P[idx, idx] = 1
    else:
        np.fill_diagonal(P, 1)

    if perm is None:
        perm = [x for x in range(L.C.dim)]

    zl = [0 for x in range(L.C.dim)]

    for m in L.m:
        if list(m.pos[perm]) <= zl:
            matrix = copy.deepcopy(m.matrix)
            pos = copy.deepcopy(m.pos)
            if np.linalg.norm(pos[perm] - zl) == 0:            
                G.add_multiplier(1/omega*P@matrix@P, pos)
            else:
                G.add_multiplier(P@matrix@P, pos)
    G.del_zero_multiplier()
    return G


@export
def upper_triangle_of(L, idx=None, perm=None, omega=1.0):
    """
    Returns the operator G with
    the multipliers

    m_G^{(y)} = 1/omega*p@m_L^{(y)}@p if y = 0, (omega is the relaxation parameter)
    m_G^{(y)} = p@m_L^{(y)}@p if y > 0,
    m_G^{(y)} = 0 if y > 0,

    with P[i,j] = 1 if i=j in idx,
                = 0 else.
                
    """

    assert isinstance(L, Operator), " "
    G = Operator(L.C)
    P = np.zeros([G.C.s[1].shape[0], G.C.s[0].shape[0]])
    if idx is not None:
        idx = list(set(idx))
        P[idx, idx] = 1
    else:
        np.fill_diagonal(P, 1)

    if perm is None:
        perm = [x for x in range(L.C.dim)]

    zl = [0 for x in range(L.C.dim)]

    for m in L.m:
        if list(m.pos[perm]) >= zl:
            matrix = copy.deepcopy(m.matrix)
            pos = copy.deepcopy(m.pos)
            if np.linalg.norm(pos[perm] - zl) == 0:                   
                G.add_multiplier(1/omega*P@matrix@P, pos)
            else:
                G.add_multiplier(P@matrix@P, pos)
    G.del_zero_multiplier()
    return G


@export
def central_multiplier_of(L, idx=None):
    """
    Returns the operator G consisting of the central multiplier of L:
        m_G^{(0)} = p@m_L^{(0)}@p,
        m_G^{(y)} = 0 if y != 0,

        with P[i,j] = 1 if i=j in idx,
            = 0 else.
    """
    assert isinstance(L, Operator), " "
    G = Operator(L.C)
    P = np.zeros([G.C.s[1].shape[0], G.C.s[0].shape[0]])
    if idx is not None:
        P[idx, idx] = 1
    else:
        np.fill_diagonal(P, 1)

    for m in L.m:
        if not linalg.norm(m.pos):
            matrix = copy.deepcopy(m.matrix)
            pos = copy.deepcopy(m.pos)
            if idx is not None:
                G.add_multiplier(P@matrix@P, pos)
            else:
                G.add_multiplier(matrix, pos)
            return G
    print("central multiplier not found. Returning 0-Operator.")
    G.del_zero_multiplier()
    return G
